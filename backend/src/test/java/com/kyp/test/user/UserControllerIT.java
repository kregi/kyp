package com.kyp.test.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIT {
  private static final String USER_URL = "/api/user";

  @Autowired
  private MockMvc mvc;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Test
  public void shouldReturnUsers() throws Exception {
    // given

    // when
    String response = mvc.perform(get(USER_URL))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    List<User> users = objectMapper.readValue(response, List.class);

    // then
    assertThat(users).isNotEmpty();
  }
}
