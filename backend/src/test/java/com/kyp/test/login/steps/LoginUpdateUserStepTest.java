package com.kyp.test.login.steps;

import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoginUpdateUserStepTest {

  @InjectMocks
  private LoginUpdateUserStep target;

  @Mock
  private LoginStep loginStep;

  @Mock
  private LoginForm loginForm;
  @Mock
  private User user;

  @Test
  public void shouldSetLoggedGivenUser() {
    // given

    // when
    target.login(user, loginForm);

    // then
    verify(user).setLogged(true);
  }

  @Test
  public void shouldSetLastLoginDateGivenUser() {
    // given

    // when
    target.login(user, loginForm);

    // then
    verify(user).setLastLogin(anyLong());
  }

  @Test
  public void shouldReturnUser() {
    // given
    given(loginStep.login(user, loginForm)).willReturn(user);

    // when
    User result = target.login(user, loginForm);

    // then
    assertThat(result).isEqualTo(user);
  }

  @Test
  public void shouldCallNextLoginStepGivenUser() {
    // given

    // when
    target.login(user, loginForm);

    // then
    verify(loginStep).login(user, loginForm);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowExceptionGivenNullUser() {
    // given

    // when
    target.login(null, loginForm);
  }
}
