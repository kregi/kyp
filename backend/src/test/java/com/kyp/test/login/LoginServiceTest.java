package com.kyp.test.login;

import com.kyp.test.login.steps.LoginStep;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceTest {
  private static final String USER_EMAIL = "1@test.com";
  private static final String NON_EXISTING_USER_EMAIL = "test@test.com";

  @InjectMocks
  private LoginService target;

  @Mock
  private UserRepository userRepository;
  @Mock
  private LoginStep loginStep;

  @Mock
  private User user;
  @Mock
  private LoginForm loginForm;

  @Before
  public void setUp() {
    given(loginForm.getEmail()).willReturn(USER_EMAIL);
    given(userRepository.findUser(USER_EMAIL)).willReturn(user);
  }

  @Test
  public void shouldCallLoginStepGivenUser() {
    // given

    // when
    target.login(loginForm);

    // then
    verify(loginStep).login(user, loginForm);
  }

  @Test
  public void shouldCallLoginStepGivenNullUser() {
    // given
    given(loginForm.getEmail()).willReturn(NON_EXISTING_USER_EMAIL);

    // when
    target.login(loginForm);

    // then
    verify(loginStep).login(null, loginForm);
  }

  @Test
  public void shouldReturnUser() {
    // given
    given(loginStep.login(user, loginForm)).willReturn(user);

    // when
    User result = target.login(loginForm);

    // then
    assertThat(result).isEqualTo(user);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowExceptionGivenNoLoginForm() {
    // given

    // when
    target.login(null);
  }
}