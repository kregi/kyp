package com.kyp.test.login.steps;

import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;
import com.kyp.test.user.UserNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoginValidateUserStepTest {

  @InjectMocks
  private LoginValidateUserStep target;

  @Mock
  private LoginStep loginStep;

  @Mock
  private User user;
  @Mock
  private LoginForm loginForm;

  @Test(expected = UserNotFoundException.class)
  public void shouldThrowExceptionGivenNullUser() {
    // given

    // when
    target.login(null, loginForm);
  }

  @Test
  public void shouldCallNextLoginStepGivenUser() {
    // given

    // when
    target.login(user, loginForm);

    // then
    verify(loginStep).login(user, loginForm);
  }

  @Test
  public void shouldReturnUser() {
    // given
    given(loginStep.login(user, loginForm)).willReturn(user);

    // when
    User result = target.login(user, loginForm);

    // then
    assertThat(result).isEqualTo(user);
  }
}