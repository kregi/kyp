package com.kyp.test.login.steps;

import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoginUpdateInRepoStepTest {

  @InjectMocks
  private LoginUpdateInRepoStep target;

  @Mock
  private UserRepository userRepository;

  @Mock
  private User user;
  @Mock
  private LoginForm loginForm;

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowExceptionGivenNullUser() {
    // given

    // when
    target.login(null, loginForm);
  }

  @Test
  public void shouldCallUpdateInRepo() {
    // given

    // when
    target.login(user, loginForm);

    // then
    verify(userRepository).update(user);
  }

  @Test
  public void shouldReturnUser() {
    // given
    given(userRepository.update(user)).willReturn(user);

    // when
    User result = target.login(user, loginForm);

    // then
    assertThat(result).isEqualTo(user);
  }
}
