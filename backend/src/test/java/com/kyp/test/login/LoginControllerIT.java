package com.kyp.test.login;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LoginControllerIT {
  private static final String LOGIN_URL = "/api/login";

  private static final String LOGGED_USER_EMAIL = "2@test.com";
  private static final String NON_EXISTING_USER_EMAIL = "nonexisting@test.com";
  private static final String NOT_LOGGED_USER_EMAIL = "3@test.com";

  @Autowired
  private MockMvc mvc;

  @Autowired
  private UserRepository userRepository;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setUp() {
    User user = userRepository.findUser(LOGGED_USER_EMAIL);
    user.setLogged(true);
    userRepository.update(user);
    user = userRepository.findUser(NOT_LOGGED_USER_EMAIL);
    user.setLogged(false);
    userRepository.update(user);
  }

  @Test
  public void shouldLoginUser() throws Exception {
    // given

    // when
    mvc.perform(post(LOGIN_URL).content(loginFormJson(loginForm(NOT_LOGGED_USER_EMAIL)))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

    // then
    assertThat(userRepository.findUser(NOT_LOGGED_USER_EMAIL).isLogged()).isTrue();
  }

  @Test
  public void shouldReturnOkGivenAlreadyLoggedUser() throws Exception {
    // given

    // when
    mvc.perform(post(LOGIN_URL).content(loginFormJson(loginForm(LOGGED_USER_EMAIL)))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

    // then
    assertThat(userRepository.findUser(LOGGED_USER_EMAIL).isLogged()).isTrue();
  }

  @Test
  public void shouldReturn404GivenNoUser() throws Exception {
    // given

    // when
    mvc.perform(post(LOGIN_URL).content(loginFormJson(loginForm(NON_EXISTING_USER_EMAIL)))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
  }

  private String loginFormJson(LoginForm loginForm) throws JsonProcessingException {
    return objectMapper.writeValueAsString(loginForm);
  }

  private LoginForm loginForm(String email) {
    return new LoginForm(email, "");
  }
}
