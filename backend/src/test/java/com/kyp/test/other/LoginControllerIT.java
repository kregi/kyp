package com.kyp.test.other;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyp.test.login.LoginForm;
import com.kyp.test.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;
import java.util.stream.IntStream;

import static com.kyp.test.other.LoginControllerIT.Action.LOGIN;
import static com.kyp.test.other.LoginControllerIT.Action.LOGOUT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LoginControllerIT {
  private static final String LOGIN_URL = "/api/login";
  private static final String LOGOUT_URL = "/api/logout";

  private static final int USERS_AMOUNT = 300;
  private static final String EMAIL_POST = "@test.com";

  @Autowired
  private MockMvc mvc;

  @Autowired
  private UserRepository userRepository;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setUp() {
    for (int i = 1; i < USERS_AMOUNT; i++) {
      userRepository.findUser(email(i)).setLogged(false);
    }
  }

  @Test
  public void testParallelLogin() {
    // when
    IntStream.range(1, USERS_AMOUNT).parallel()
            .forEach(i -> {
              try {
                login(email(i));
              } catch (Exception e) {
                e.printStackTrace();
              }
            });

    // then
    for (int i = 1; i < USERS_AMOUNT; i++) {
      assertThat(userRepository.findUser(email(i)).isLogged()).isTrue();
    }
  }

  @Test
  public void testLoginLogout() {
    // given
    List<Action> actions = Arrays.asList(LOGIN, LOGOUT, LOGIN, LOGOUT, LOGIN, LOGOUT, LOGIN);

    // when
    IntStream.range(1, USERS_AMOUNT).parallel()
            .forEach(i -> {
              for (Action action: actions) {
                try {
                  perform(action, email(i));
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            });

    // then
    for (int i = 1; i < USERS_AMOUNT; i++) {
      assertThat(userRepository.findUser(email(i)).isLogged()).isTrue();
    }
  }

  private String email(int i) {
    return i + EMAIL_POST;
  }

  private void perform(Action action, String email) throws Exception {
    if (action == LOGIN) {
      login(email);
    } else {
      logout(email);
    }
  }

  public void login(String email) throws Exception {
    mvc.perform(post(LOGIN_URL).content(loginFormJson(loginForm(email)))
            .contentType(MediaType.APPLICATION_JSON));
  }

  private void logout(String email) throws Exception {
    mvc.perform(post(LOGOUT_URL).content(email)
            .contentType(MediaType.TEXT_PLAIN));
  }

  private String loginFormJson(LoginForm loginForm) throws JsonProcessingException {
    return objectMapper.writeValueAsString(loginForm);
  }

  private LoginForm loginForm(String email) {
    return new LoginForm(email, "");
  }

  public static enum Action {
    LOGIN, LOGOUT
  }
}
