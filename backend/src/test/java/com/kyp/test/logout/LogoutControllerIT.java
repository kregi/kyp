package com.kyp.test.logout;

import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LogoutControllerIT {
  private static final String LOGOUT_URL = "/api/logout";

  private static final String LOGGED_USER_EMAIL = "2@test.com";
  private static final String NON_EXISTING_USER_EMAIL = "nonexisting@test.com";
  private static final String NOT_LOGGED_USER_EMAIL = "3@test.com";

  @Autowired
  private MockMvc mvc;

  @Autowired
  private UserRepository userRepository;

  @Before
  public void setUp() {
    User user = userRepository.findUser(LOGGED_USER_EMAIL);
    user.setLogged(true);
    userRepository.update(user);
    user = userRepository.findUser(NOT_LOGGED_USER_EMAIL);
    user.setLogged(false);
    userRepository.update(user);
  }

  @Test
  public void shouldLogoutUser() throws Exception {
    // given

    // when
    mvc.perform(post(LOGOUT_URL).content(LOGGED_USER_EMAIL)
            .contentType(MediaType.TEXT_PLAIN))
            .andExpect(status().isOk());

    // then
    assertThat(userRepository.findUser(LOGGED_USER_EMAIL).isLogged()).isFalse();
  }

  @Test
  public void shouldReturnOkGivenNonLoggedUser() throws Exception {
    // given

    // when
    mvc.perform(post(LOGOUT_URL).content(NOT_LOGGED_USER_EMAIL)
            .contentType(MediaType.TEXT_PLAIN))
            .andExpect(status().isOk());

    // then
    assertThat(userRepository.findUser(NOT_LOGGED_USER_EMAIL).isLogged()).isFalse();
  }

  @Test
  public void shouldReturn404GivenNoUser() throws Exception {
    // given

    // when
    mvc.perform(post(LOGOUT_URL).content(NON_EXISTING_USER_EMAIL)
            .contentType(MediaType.TEXT_PLAIN))
            .andExpect(status().isNotFound());
  }
}
