package com.kyp.test.logout;

import com.kyp.test.logout.steps.LogoutStep;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LogoutServiceTest {
  private static final String USER_EMAIL = "1@test.com";
  private static final String NON_EXISTING_USER_MAIL = "test@test.com";

  @InjectMocks
  private LogoutService target;

  @Mock
  private UserRepository userRepository;
  @Mock
  private LogoutStep logoutStep;

  @Mock
  private User user;

  @Before
  public void setUp() {
    given(userRepository.findUser(USER_EMAIL)).willReturn(user);
    given(logoutStep.logout(user)).willReturn(user);
  }

  @Test
  public void shouldCallStepWithUser() {
    // given

    // when
    target.logout(USER_EMAIL);

    // then
    verify(logoutStep).logout(user);
  }

  @Test
  public void shouldReturnUser() {
    // given

    // when
    User result = target.logout(USER_EMAIL);

    // then
    assertThat(result).isEqualTo(user);
  }

  @Test
  public void shouldCallStepGivenNoUser() {
    // given

    // when
    target.logout(NON_EXISTING_USER_MAIL);

    // then
    verify(logoutStep).logout(null);
  }

}