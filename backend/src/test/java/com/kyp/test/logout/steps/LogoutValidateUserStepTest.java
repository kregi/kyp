package com.kyp.test.logout.steps;

import com.kyp.test.user.User;
import com.kyp.test.user.UserNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LogoutValidateUserStepTest {

  @InjectMocks
  private LogoutValidateUserStep target;

  @Mock
  private LogoutStep logoutStep;

  @Mock
  private User user;

  @Test(expected = UserNotFoundException.class)
  public void shouldThrowExceptionGivenNullUser() {
    // given

    // when
    target.logout(null);
  }

  @Test
  public void shouldNotCallNextStepGivenNullUser() {
    // given

    try {
      // when
      target.logout(null);
      fail("Exception should be thrown");
    } catch (UserNotFoundException e) {
      // then
      verify(logoutStep, times(0)).logout(any(User.class));
    }
  }

  @Test
  public void shouldCallNextStepGivenUser() {
    // given

    // when
    target.logout(user);

    // then
    verify(logoutStep).logout(user);
  }

  @Test
  public void shouldReturnUser() {
    // given
    given(logoutStep.logout(user)).willReturn(user);

    // when
    User result = target.logout(user);

    // then
    assertThat(result).isEqualTo(user);
  }
}
