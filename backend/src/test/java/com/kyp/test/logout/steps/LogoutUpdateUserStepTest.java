package com.kyp.test.logout.steps;

import com.kyp.test.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LogoutUpdateUserStepTest {

  @InjectMocks
  private LogoutUpdateUserStep target;

  @Mock
  private LogoutStep logoutStep;

  @Mock
  private User user;

  @Test
  public void shouldSetLoggedFalseOnLogout() {
    // given

    // when
    target.logout(user);

    // then
    verify(user).setLogged(false);
  }

  @Test
  public void shouldReturnUser() {
    // given
    given(logoutStep.logout(user)).willReturn(user);

    // when
    User result = target.logout(user);

    // then
    assertThat(result).isEqualTo(user);
  }

  @Test
  public void shouldCallNextStepGivenUser() {
    // given

    // when
    target.logout(user);

    // then
    verify(logoutStep).logout(user);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowExceptionGivenNullUser() {
    // given

    // when
    target.logout(null);
  }
}
