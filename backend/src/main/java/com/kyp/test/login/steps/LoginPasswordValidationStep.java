package com.kyp.test.login.steps;

import lombok.AllArgsConstructor;
import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;

@AllArgsConstructor
public class LoginPasswordValidationStep implements LoginStep {
  private final LoginStep nextStep;

  @Override
  public User login(User user, LoginForm loginForm) {
    // validate password, throw exception when password is incorrect
    return nextStep.login(user, loginForm);
  }
}
