package com.kyp.test.login.steps;

import lombok.AllArgsConstructor;
import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.springframework.util.Assert;

@AllArgsConstructor
public class LoginUpdateInRepoStep implements LoginStep {

  private UserRepository userRepository;

  @Override
  public User login(User user, LoginForm loginForm) {
    Assert.notNull(user, "User cannot be null");
    return userRepository.update(user);
  }
}
