package com.kyp.test.login.steps;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;
import com.kyp.test.user.UserNotFoundException;

@Slf4j
@AllArgsConstructor
public class LoginValidateUserStep implements LoginStep {

  private final LoginStep nextStep;

  @Override
  public User login(User user, LoginForm loginForm) {
    if (user == null) {
      log.info("User {} not found in db", loginForm.getEmail());
      throw new UserNotFoundException();
    }
    return nextStep.login(user, loginForm);
  }
}
