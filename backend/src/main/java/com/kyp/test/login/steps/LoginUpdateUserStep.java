package com.kyp.test.login.steps;

import lombok.AllArgsConstructor;
import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;
import org.springframework.util.Assert;

@AllArgsConstructor
public class LoginUpdateUserStep implements LoginStep {
  private LoginStep nextStep;

  @Override
  public User login(User user, LoginForm loginForm) {
    Assert.notNull(user, "User cannot be null");
    user.setLogged(true);
    user.setLastLogin(System.currentTimeMillis());
    return nextStep.login(user, loginForm);
  }
}
