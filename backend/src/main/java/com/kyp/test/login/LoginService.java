package com.kyp.test.login;

import lombok.AllArgsConstructor;
import com.kyp.test.login.steps.LoginStep;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.springframework.util.Assert;

@AllArgsConstructor
public class LoginService {

  private final UserRepository userRepository;
  private final LoginStep nextStep;

  public User login(LoginForm loginForm) {
    Assert.notNull(loginForm, "Login form cannot be null");
    User user = userRepository.findUser(loginForm.getEmail());
    return nextStep.login(user, loginForm);
  }
}
