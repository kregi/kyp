package com.kyp.test.login;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.kyp.test.user.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
@AllArgsConstructor
@Slf4j
public class LoginController {

  private final LoginService loginService;

  @PostMapping
  public User login(@RequestBody LoginForm loginForm) {
    log.info("Login request for {}", loginForm.getEmail());
    return loginService.login(loginForm);
  }
}
