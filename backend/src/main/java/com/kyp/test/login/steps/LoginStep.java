package com.kyp.test.login.steps;

import com.kyp.test.login.LoginForm;
import com.kyp.test.user.User;

public interface LoginStep {
  User login(User user, LoginForm loginForm);
}
