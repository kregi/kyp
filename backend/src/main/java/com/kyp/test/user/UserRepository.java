package com.kyp.test.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class UserRepository {
  private final ConcurrentHashMap<String, User> users = new ConcurrentHashMap<>();

  public User save(User user) {
    log.info("Save user {}", user);
    users.put(user.getEmail(), user.toBuilder().build());
    return user;
  }

  public User update(User user) {
    log.info("Update user {}", user);
    users.replace(user.getEmail(), user);
    return user;
  }

  public Set<User> findAll() {
    return users.values().stream()
            .map(user -> user.toBuilder().build())
            .collect(Collectors.toSet());
  }

  public User findUser(String email) {
    User inRepo = users.get(email);
    User user = null;
    if (inRepo != null) {
      user = inRepo.toBuilder().build();
    }
    return user;
  }
}
