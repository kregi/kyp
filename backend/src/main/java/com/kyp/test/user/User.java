package com.kyp.test.user;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder(toBuilder = true)
public class User {
  private String email;

  private String firstName;
  private String lastName;

  private boolean logged;
  private Long lastLogin;
}
