package com.kyp.test.logout;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.kyp.test.user.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/logout")
@AllArgsConstructor
@Slf4j
public class LogoutController {

  private final LogoutService logoutService;

  @PostMapping
  public User login(@RequestBody String email) {
    log.info("Logout request for {}", email);
    return logoutService.logout(email);
  }
}
