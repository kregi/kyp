package com.kyp.test.logout;

import lombok.AllArgsConstructor;
import com.kyp.test.logout.steps.LogoutStep;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;

@AllArgsConstructor
public class LogoutService {
  private final UserRepository userRepository;
  private final LogoutStep logoutStep;

  public User logout(String email) {
    User user = userRepository.findUser(email);
    return logoutStep.logout(user);
  }
}
