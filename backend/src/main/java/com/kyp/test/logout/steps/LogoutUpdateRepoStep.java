package com.kyp.test.logout.steps;

import lombok.AllArgsConstructor;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.springframework.util.Assert;

@AllArgsConstructor
public class LogoutUpdateRepoStep implements LogoutStep {
  private UserRepository userRepository;

  @Override
  public User logout(User user) {
    Assert.notNull(user, "User cannot be null");
    return userRepository.update(user);
  }
}
