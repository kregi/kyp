package com.kyp.test.logout.steps;

import lombok.AllArgsConstructor;
import com.kyp.test.user.User;
import org.springframework.util.Assert;

@AllArgsConstructor
public class LogoutUpdateUserStep implements LogoutStep {
  private LogoutStep nextStep;

  @Override
  public User logout(User user) {
    Assert.notNull(user, "User cannot be null");
    user.setLogged(false);
    return nextStep.logout(user);
  }
}
