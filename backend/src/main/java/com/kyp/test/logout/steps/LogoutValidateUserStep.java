package com.kyp.test.logout.steps;

import lombok.AllArgsConstructor;
import com.kyp.test.user.User;
import com.kyp.test.user.UserNotFoundException;

@AllArgsConstructor
public class LogoutValidateUserStep implements LogoutStep {
  private final LogoutStep nextStep;

  @Override
  public User logout(User user) {
    if (user == null) {
      throw new UserNotFoundException();
    }
    return nextStep.logout(user);
  }
}
