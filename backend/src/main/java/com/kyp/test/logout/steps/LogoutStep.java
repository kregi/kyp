package com.kyp.test.logout.steps;

import com.kyp.test.user.User;

public interface LogoutStep {
  User logout(User user);
}
