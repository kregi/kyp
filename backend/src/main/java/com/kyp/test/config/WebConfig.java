package com.kyp.test.config;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;
import javax.validation.Valid;

import static java.util.Collections.emptyList;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class WebConfig
        implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

  @Bean
  @ConfigurationProperties(prefix = "kyp.web")
  ApplicationProperties webApplicationProperties() {
    return new ApplicationProperties();
  }

  @Override
  public void customize(ConfigurableServletWebServerFactory factory) {
    MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
    mappings.add("html", MediaType.TEXT_HTML_VALUE + ";charset=utf-8");
    mappings.add("json", MediaType.TEXT_HTML_VALUE + ";charset=utf-8");
    factory.setMimeMappings(mappings);
  }

  @Bean
  CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration cors = webApplicationProperties().getCors();
    List<String> allowedOrigins = cors != null ? cors.getAllowedOrigins() : emptyList();

    if (allowedOrigins != null && !allowedOrigins.isEmpty()) {
      log.debug("Registering CORS filter");
      source.registerCorsConfiguration("/api/**", cors);
    }
    return new CorsFilter(source);
  }

  @Data
  @Validated
  public static class ApplicationProperties {

    @Valid
    @NestedConfigurationProperty
    private final CorsConfiguration cors = new CorsConfiguration();
  }
}
