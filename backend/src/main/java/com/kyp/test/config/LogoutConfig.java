package com.kyp.test.config;

import com.kyp.test.logout.LogoutService;
import com.kyp.test.logout.steps.LogoutUpdateRepoStep;
import com.kyp.test.logout.steps.LogoutUpdateUserStep;
import com.kyp.test.logout.steps.LogoutValidateUserStep;
import com.kyp.test.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogoutConfig {

  @Bean
  @Autowired
  public LogoutService logoutService(UserRepository userRepository,
                                     LogoutValidateUserStep logoutValidateUserStep) {
    return new LogoutService(userRepository, logoutValidateUserStep);
  }

  @Bean
  @Autowired
  public LogoutValidateUserStep logoutValidateUserStep(LogoutUpdateUserStep logoutUpdateUserStep) {
    return new LogoutValidateUserStep(logoutUpdateUserStep);
  }

  @Bean
  @Autowired
  public LogoutUpdateUserStep logoutUpdateUserStep(LogoutUpdateRepoStep updateRepoStep) {
    return new LogoutUpdateUserStep(updateRepoStep);
  }

  @Bean
  @Autowired
  public LogoutUpdateRepoStep updateRepoStep(UserRepository userRepository) {
    return new LogoutUpdateRepoStep(userRepository);
  }
}
