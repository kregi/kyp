package com.kyp.test.config;

import lombok.AllArgsConstructor;
import com.kyp.test.user.User;
import com.kyp.test.user.UserRepository;
import org.springframework.context.annotation.Configuration;

import java.util.Random;
import java.util.stream.IntStream;
import javax.annotation.PostConstruct;

@Configuration
@AllArgsConstructor
public class TestUsers {

  private static final String TEST_EMAIL_BOX = "@test.com";
  private static final Random RANDOM = new Random();

  private final UserRepository userRepository;

  @PostConstruct
  public void addUsers() {
    IntStream.range(1, 1000).forEach(i -> {
      String name = String.valueOf(i);
      userRepository.save(User.builder()
              .email(i + TEST_EMAIL_BOX)
              .firstName(name)
              .lastName(name)
              .lastLogin(System.currentTimeMillis() - Math.abs(RANDOM.nextLong()) % 100000000000L)
              .build());
    });
  }
}
