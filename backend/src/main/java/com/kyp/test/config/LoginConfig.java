package com.kyp.test.config;

import lombok.AllArgsConstructor;
import com.kyp.test.login.LoginService;
import com.kyp.test.login.steps.LoginPasswordValidationStep;
import com.kyp.test.login.steps.LoginUpdateInRepoStep;
import com.kyp.test.login.steps.LoginUpdateUserStep;
import com.kyp.test.login.steps.LoginValidateUserStep;
import com.kyp.test.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class LoginConfig {

  @Bean
  @Autowired
  public LoginService loginService(UserRepository userRepository,
                                   LoginValidateUserStep loginValidateUserStep) {
    return new LoginService(userRepository, loginValidateUserStep);
  }

  @Bean
  @Autowired
  public LoginValidateUserStep loginValidateUserStep(
          LoginPasswordValidationStep loginPasswordValidationStep) {
    return new LoginValidateUserStep(loginPasswordValidationStep);
  }

  @Bean
  @Autowired
  public LoginPasswordValidationStep loginPasswordValidationStep(
          LoginUpdateUserStep loginUpdateUserStep) {
    return new LoginPasswordValidationStep(loginUpdateUserStep);
  }

  @Bean
  @Autowired
  public LoginUpdateUserStep loginUpdateUserStep(LoginUpdateInRepoStep loginUpdateInRepoStep) {
    return new LoginUpdateUserStep(loginUpdateInRepoStep);
  }

  @Bean
  @Autowired
  public LoginUpdateInRepoStep loginUpdateInRepoStep(UserRepository userRepository) {
    return new LoginUpdateInRepoStep(userRepository);
  }
}
