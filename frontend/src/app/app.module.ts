import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {UsersComponent} from './users/users.component';
import {MatTableModule} from '@angular/material';
import {MatSortModule} from '@angular/material/sort'
import {UserService} from './service/user/user.service';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent, HomeComponent, UsersComponent
  ],
  imports: [
    AppRoutingModule, BrowserModule, BrowserAnimationsModule, HttpClientModule, MatTableModule, MatSortModule,
    MatPaginatorModule, ReactiveFormsModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
