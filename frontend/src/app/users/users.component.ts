import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../service/user/User';
import {UserService} from '../service/user/user.service';
import {MatSort, MatTableDataSource} from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['email', 'firstName', 'lastName', 'lastLogin', 'logged'];
  dataSource = new MatTableDataSource<User>();

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.userService.getUsers()
      .subscribe((data: User[]) => {
          this.dataSource.data = data;
        },
        (error: any) => console.log(error)
      );
  }

  get allUsers(): number {
    return this.dataSource.data.length;
  }

  get loggedUsers(): number {
    return this.dataSource.data
      .filter(user => user.logged)
      .reduce((sum, current) => sum + 1, 0);
  }
}
