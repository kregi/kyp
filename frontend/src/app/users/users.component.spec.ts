import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UsersComponent} from './users.component';
import {Observable} from 'rxjs/Observable';
import {USERS} from '../test/users';
import {UserService} from '../service/user/user.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {of} from 'rxjs/observable/of';

class UserServiceMock {
  getUsers() {
    return of(USERS);
  }
}

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatPaginatorModule, MatTableModule],
      declarations: [UsersComponent],
      providers: [
        {provide: UserService, useClass: UserServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#LoadUsers', () => {
    it('should load users', () => {
      component.ngOnInit();
      expect(component.dataSource.data).toEqual(USERS);
    });
    it('should return users size', () => {
      component.ngOnInit();
      expect(component.allUsers).toEqual(USERS.length);
    });
    it('should return logged users size', () => {
      component.ngOnInit();
      expect(component.loggedUsers).toEqual(3);
    });
  });
});
