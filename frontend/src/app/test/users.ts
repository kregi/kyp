import {User} from '../service/user/User';

export const USERS: User[] = [
  {
    email: '1@test.com',
    firstName: '1',
    lastName: '1',
    lastLogin: 0,
    logged: false
  },
  {
    email: '5@test.com',
    firstName: '5',
    lastName: '5',
    lastLogin: 0,
    logged: false
  },
  {
    email: '3@test.com',
    firstName: '3',
    lastName: '3',
    lastLogin: 0,
    logged: false
  },
  {
    email: '4@test.com',
    firstName: '4',
    lastName: '4',
    lastLogin: 0,
    logged: false
  },
  {
    email: '6@test.com',
    firstName: '6',
    lastName: '6',
    lastLogin: 0,
    logged: true
  },
  {
    email: '7@test.com',
    firstName: '7',
    lastName: '7',
    lastLogin: 0,
    logged: true
  },
  {
    email: '2@test.com',
    firstName: '2',
    lastName: '2',
    lastLogin: 0,
    logged: true
  }
];
