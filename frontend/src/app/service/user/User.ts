export interface User {
  email: string;
  firstName: string;
  lastName: string;
  logged: boolean;
  lastLogin: number;
}
