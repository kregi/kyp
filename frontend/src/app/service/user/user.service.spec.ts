import {getTestBed, TestBed} from '@angular/core/testing';

import { UserService } from './user.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {USERS} from '../../test/users';

describe('UserService', () => {
  let injector;
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    injector = getTestBed();
    service = injector.get(UserService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  describe('#getUsers', () => {
    it('should return an Observable<User[]>', () => {
      service.getUsers().subscribe(users => {
        expect(users.length).toBe(USERS.length);
        expect(users).toEqual(USERS);
      });

      const req = httpMock.expectOne(`${service.apiUrl}`);
      expect(req.request.method).toBe('GET');
      req.flush(USERS);
    });
  });
});
