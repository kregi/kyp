import {getTestBed, TestBed} from '@angular/core/testing';

import {AuthenticationService} from './authentication.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {USERS} from '../../test/users';

describe('AuthenticationService', () => {
  const EMAIL = '1@test.com';
  const PASS = 'pass';

  let injector;
  let service: AuthenticationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    injector = getTestBed();
    service = injector.get(AuthenticationService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    expect(service).toBeTruthy();
  });

  describe('#login', () => {
    it('should set user when login', () => {
      const user = USERS[0];
      service.login(EMAIL, PASS).subscribe(result => {
        expect(result).toBe(user);
        expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBe(JSON.stringify(user));
      });

      const req = httpMock.expectOne(`${service.apiUrl}`);
      expect(req.request.method).toBe('POST');
      req.flush(user);
    });
  });

  describe('#logout', () => {
    it('should remove user on logout', () => {
      const user = USERS[0];
      localStorage.setItem(AuthenticationService.CURRENT_USER, JSON.stringify(user));
      service.logout();

      expect(localStorage.getItem(AuthenticationService.CURRENT_USER)).toBeNull();
    })
  });
});
